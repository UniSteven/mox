package me.unisteven.mox.playerdata;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import me.unisteven.mox.main.Mox;

public class PlayerFileManager {
	private Mox plugin;
	private List<PlayerFile> list = new ArrayList<>();

	public PlayerFileManager(Mox plugin) {
		this.plugin = plugin;
	}

	private List<PlayerFile> loadPlayerFiles() {
		List<PlayerFile> list = new ArrayList<>();
		for (Player p : Bukkit.getOnlinePlayers()) {
		loadPlayerFile(p);
		}
		return list;

	}

	private PlayerFile getPlayer(FileConfiguration da, Player p) {
		String name = da.getString(".Name");
		int RedstonePlaced = da.getInt(".RedstonePlaced");
		PlayerFile de = new PlayerFile(p, name, RedstonePlaced,plugin);
		return de;
	}


	public void init() {
		list = loadPlayerFiles();
	}

	public List<PlayerFile> getPlayerFiles() {
		return list;
	}

	public PlayerFile getPlayerFile(Player p) {
		return getPlayerFiles().stream().filter(d -> d.getP() == p).findFirst().orElse(null);
	}

	private boolean fileExistCheck(File file, Player p) {
		try {
			File dir = new File(plugin.getDataFolder().getPath() + "/players");
			if (!dir.exists()) {
				dir.mkdirs();
			}
			if (!file.exists()) {
				plugin.getLogger().info(p.getUniqueId().toString() + ".yml not found, creating!");
				file.createNewFile();
				PlayerFile de = new PlayerFile(p, p.getName(), 0, plugin);
				de.save(p);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return true;
	}

	public boolean loadPlayerFile(Player p) {
		File file = new File(plugin.getDataFolder() + "/players", p.getUniqueId().toString() + ".yml");
		if(fileExistCheck(file, p)){
			FileConfiguration da = YamlConfiguration.loadConfiguration(file);
			list.add(getPlayer(da, p));
		}else{
			FileConfiguration da = YamlConfiguration.loadConfiguration(file);
			list.add(getPlayer(da, p));
		}
		return true;
	}

	public void unloadPlayerFile(Player p) {
		getPlayerFile(p).save(p);
		PlayerFile pf = getPlayerFile(p);
		List<PlayerFile> li = list;
		li.remove(pf);
		list = li;
	}
}
