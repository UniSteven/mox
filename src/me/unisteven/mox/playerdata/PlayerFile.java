package me.unisteven.mox.playerdata;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import me.unisteven.mox.main.Mox;

public class PlayerFile {
	Mox plugin;
	private Player p;
	private String name;
	private int RedstonePlaced;
	public PlayerFile(Player p, String name, int RedstonePlaced, Mox plugin) {
		super();
		this.p = p;
		this.name = name;
		this.RedstonePlaced = RedstonePlaced;
		this.plugin = plugin;
	}

	public String getName() {
		return name;
	}

	public int getRedstonePlaced() {
		return RedstonePlaced;
	}

	public void setP(Player p) {
		this.p = p;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRedstonePlaced(int RedstonePlaced) {
		this.RedstonePlaced = RedstonePlaced;
	}


	public Player getP() {
		return p;
	}

	public void save(Player p) {
		File file = new File(plugin.getDataFolder() + "/players", p.getUniqueId().toString() + ".yml");
		FileConfiguration da = YamlConfiguration.loadConfiguration(file);
		da.set(".Name", name);
		da.set(".RedstonePlaced", RedstonePlaced);
		try {
			da.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


}
