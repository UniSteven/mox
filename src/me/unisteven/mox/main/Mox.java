package me.unisteven.mox.main;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.unisteven.mox.config.CheckConfig;
import me.unisteven.mox.listner.BlockListener;
import me.unisteven.mox.listner.DropItemListener;
import me.unisteven.mox.listner.JoinListener;
import me.unisteven.mox.listner.LeaveListener;
import me.unisteven.mox.playerdata.PlayerFile;
import me.unisteven.mox.playerdata.PlayerFileManager;

public class Mox extends JavaPlugin{
	private PlayerFileManager playerFileManager;
	
	public void onEnable(){
		new CheckConfig(this).createConfig();
		this.playerFileManager = new PlayerFileManager(this);
		playerFileManager.init();
		Bukkit.getPluginManager().registerEvents(new JoinListener(this), this);
		Bukkit.getPluginManager().registerEvents(new LeaveListener(this), this);
		Bukkit.getPluginManager().registerEvents(new BlockListener(this), this);
		Bukkit.getPluginManager().registerEvents(new DropItemListener(this), this);
		for (Player p : Bukkit.getOnlinePlayers()) {
			if(playerFileManager.loadPlayerFile(p)){
				Bukkit.getLogger().log(Level.INFO, "loading player: " + p.getName());
			}
		}
	}
	
	public void onDisable(){
		for (Player p : Bukkit.getOnlinePlayers()) {
			PlayerFile pf = playerFileManager.getPlayerFile(p);
			pf.save(p);
		}
		
	}
	
	public PlayerFileManager getPlayerFileManager() {
		return playerFileManager;
	}
}
