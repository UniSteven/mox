package me.unisteven.mox.listner;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import me.unisteven.mox.main.Mox;

public class LeaveListener implements Listener {
	Mox plugin;

	public LeaveListener(Mox plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent e){
		plugin.getPlayerFileManager().unloadPlayerFile(e.getPlayer());
	}
}
