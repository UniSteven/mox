package me.unisteven.mox.listner;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import me.unisteven.mox.main.Mox;
import me.unisteven.mox.playerdata.PlayerFile;
import me.unisteven.mox.playerdata.PlayerFileManager;

public class BlockListener implements Listener{
	Mox plugin;

	public BlockListener(Mox plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e){
		if(e.getBlock().getType().equals(Material.REDSTONE_TORCH_ON)){
			if(plugin.getConfig().getBoolean("RedstoneLimiter")){
				if(e.getPlayer().hasPermission("Mox.Redstone.limit")){
					if(e.getPlayer().isOp()){
						return;
					}
					PlayerFileManager pfm = plugin.getPlayerFileManager();
					PlayerFile pf = pfm.getPlayerFile(e.getPlayer());
					if(pf.getRedstonePlaced() >= plugin.getConfig().getInt("RedstoneLimit")){
						e.setCancelled(true);
						e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("LimitMessage")
								.replace("%ammount%", plugin.getConfig().getInt("RedstoneLimit") + "")));
					}else{
						pf.setRedstonePlaced(pf.getRedstonePlaced() + 1);
						plugin.getLogger().log(Level.INFO, e.getPlayer().getName() + " has Placed a restone Wire");
					}
				}else{
					e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("NoPerm")));
					e.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		int count = 0;
		if(e.getBlock().getType().equals(Material.REDSTONE_TORCH_ON) || e.getBlock().getType().equals(Material.REDSTONE_TORCH_OFF) ||
				e.getBlock().getLocation().add(0, 1, 0).getBlock().getType().equals(Material.REDSTONE_TORCH_ON)|| e.getBlock().getLocation().add(0, 1, 0).getBlock().getType().equals(Material.REDSTONE_TORCH_OFF) ||
				e.getBlock().getLocation().add(1, 0, 0).getBlock().getType().equals(Material.REDSTONE_TORCH_ON) ||e.getBlock().getLocation().add(1, 0, 0).getBlock().getType().equals(Material.REDSTONE_TORCH_OFF) ||
				e.getBlock().getLocation().add(0, 0, 1).getBlock().getType().equals(Material.REDSTONE_TORCH_ON) ||e.getBlock().getLocation().add(0, 0, 1).getBlock().getType().equals(Material.REDSTONE_TORCH_OFF) ||
				e.getBlock().getLocation().add(-1, 0, 0).getBlock().getType().equals(Material.REDSTONE_TORCH_ON)||e.getBlock().getLocation().add(-1, 0, 0).getBlock().getType().equals(Material.REDSTONE_TORCH_OFF) ||
				e.getBlock().getLocation().add(0, 0, -1).getBlock().getType().equals(Material.REDSTONE_TORCH_ON) ||e.getBlock().getLocation().add(0, 0, -1).getBlock().getType().equals(Material.REDSTONE_TORCH_OFF)){
			
			if(e.getBlock().getLocation().add(0, 1, 0).getBlock().getType().equals(Material.REDSTONE_TORCH_ON) || e.getBlock().getLocation().add(0, 1, 0).getBlock().getType().equals(Material.REDSTONE_TORCH_OFF)){
				count++;
			}
			if(e.getBlock().getLocation().add(0, 0, -1).getBlock().getType().equals(Material.REDSTONE_TORCH_ON) || e.getBlock().getLocation().add(-1, 0, 0).getBlock().getType().equals(Material.REDSTONE_TORCH_OFF)){
				count++;
			}
			if(e.getBlock().getLocation().add(1, 0, 0).getBlock().getType().equals(Material.REDSTONE_TORCH_ON) || e.getBlock().getLocation().add(1, 0, 0).getBlock().getType().equals(Material.REDSTONE_TORCH_OFF)){
				count++;
			}
			if(e.getBlock().getLocation().add(0, 0, 1).getBlock().getType().equals(Material.REDSTONE_TORCH_ON) || e.getBlock().getLocation().add(0, 0, 1).getBlock().getType().equals(Material.REDSTONE_TORCH_OFF)){
				count++;
			}
			if(e.getBlock().getLocation().add(-1, 0, 0).getBlock().getType().equals(Material.REDSTONE_TORCH_ON) || e.getBlock().getLocation().add(-1, 0, 0).getBlock().getType().equals(Material.REDSTONE_TORCH_OFF)){
				count++;
			}
			if(e.getBlock().getType().equals(Material.REDSTONE_TORCH_ON) || e.getBlock().getType().equals(Material.REDSTONE_TORCH_OFF)){
				count++;
			}
			if(plugin.getConfig().getBoolean("RedstoneLimiter")){
				if(e.getPlayer().hasPermission("Mox.Redstone.limit")){
					PlayerFileManager pfm = plugin.getPlayerFileManager();
					PlayerFile pf = pfm.getPlayerFile(e.getPlayer());
					if(!(pf.getRedstonePlaced() < 1)){
						pf.setRedstonePlaced(pf.getRedstonePlaced() - count);
						plugin.getLogger().log(Level.INFO, e.getPlayer().getName() + " has removed a restone Wire");
					}
					
				}
			}
		}
	}
	

}
