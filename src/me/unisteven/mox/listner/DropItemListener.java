package me.unisteven.mox.listner;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import me.unisteven.mox.main.Mox;

public class DropItemListener implements Listener{
	
	Mox plugin;

	public DropItemListener(Mox plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onItemDrop(PlayerDropItemEvent e){
		if(!e.getPlayer().hasPermission("Mox.drop")){
			e.setCancelled(true);
		}
		
	}
	

}
