package me.unisteven.mox.listner;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.unisteven.mox.main.Mox;
import me.unisteven.mox.playerdata.PlayerFileManager;

public class JoinListener implements Listener{
	Mox plugin;
	public JoinListener(Mox plugin) {
		this.plugin = plugin;
	}
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		PlayerFileManager pfm = plugin.getPlayerFileManager();
		pfm.loadPlayerFile(e.getPlayer());
		//PlayerFile pf = pfm.getPlayerFile(e.getPlayer());
	}

}
