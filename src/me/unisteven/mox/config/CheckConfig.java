package me.unisteven.mox.config;

import java.io.File;

import me.unisteven.mox.main.Mox;

public class CheckConfig {
	Mox plugin;
	
	public CheckConfig(Mox plugin) {
		this.plugin = plugin;
	}

	public void createConfig() {
		try {
			if (!plugin.getDataFolder().exists()) {
				plugin.getDataFolder().mkdirs();
			}
			File file = new File(plugin.getDataFolder(), "config.yml");
			if (!file.exists()) {
				plugin.getLogger().info("Config.yml not found, creating!");
				plugin.saveDefaultConfig();
			} else {
				plugin.getLogger().info("Config.yml found, loading!");
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

	}
}
